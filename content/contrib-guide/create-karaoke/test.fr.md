+++
title = "Tester votre karaoké"
weight = 3
+++

Parce qu'avant d'intégrer votre karaoké dans le logiciel, il vaut mieux faire une petite vérification.

1. Ouvrez votre lecteur multimédia favori (VLC, Media Player Classic, SMPlayer, Pot Player...) et chargez votre vidéo.
2. Chargez les sous-titres en ajoutant le fichier `.ass` que vous venez de créer.
3. Vérifiez que les paroles s'affichent bien à l'écran aux bons moments.
4. Bougez vos lèvres, votre langue et expirez pour produire les paroles affichées à l'écran avec votre bouche.
    - Essayez de coller à l'intonation du chanteur ou de la chanteuse. *Beuglez s'il le faut.*
5. *(optionnel)* Si vous avez reçu une amende pour tapage nocturne ou diurne, cela signifie que votre karaoké est très efficace.

Si vous n'êtes pas satisfait du résultat, [modifiez votre fichier de sous-titres](../editkaraoke) et recommencez le test.

Si le résultat vous convient, vous pouvez passer à la création du [fichier de métadonnées de votre kara](../../karafile).

[Consultez cette section](../editkaraoke) ainsi que [celle-ci](../troubleshoot) pour vérifier si vous ne pouvez pas résoudre certains problèmes.

___

Une fois que votre karaoké est au poil, vous pouvez passer à [l'intégration du karaoké](../../karafile).

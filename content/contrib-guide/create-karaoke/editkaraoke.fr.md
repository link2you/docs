+++
title = "Modifier votre karaoké"
weight = 6
+++

## Apporter des changements à votre fichier de paroles

Vous souhaitez modifier le karaoké que vous avez fait ? Rien de plus simple :

Dans le Panneau Système de Karaoke Mugen, rendez-vous sur la liste des karaokés. Vous trouverez un bouton pour éditer les paroles qui ouvrira directement Aegisub avec le bon fichier .ass et les bons liens vers les vidéos.

Vous pouvez aussi ouvrir le fichier .ass vous-mêmes, et y déposer la vidéo associée manuellement. Pensez à changer la résolution à 0 x 0 dans "**Propriétés**".

- Sélectionnez toutes les lignes **blanches** qui ont le style "fx" et supprimez-les.
- Sélectionnez toutes les lignes **bleutées** restantes *sauf la ligne de script*, et décochez la case "**Commentaire**" qui se trouve en dessous du spectre audio.

![ass](/images/creation/DeleteLines.gif)

- Vous voilà revenu juste avant le moment où vous avez appliqué le script.
- Faites vos modifications
- **Ré-appliquez le modèle karaoké** dans l'onglet "**Automatisme**"
- Supprimez les styles inutilisés + furigana en vous rendant dans l'onglet "**Sous-titre**" > "**Gestionnaire de styles**"
- Sauvegardez !

## Changer la vidéo d'un karaoké

- Lancez Karaoke Mugen
- Allez dans **Système**
- Allez dans l'onglet **Karaokes** puis **Liste**
- Cherchez votre karaoké et appuyez sur le bouton d'édition à droite
- Remplacez la vidéo par la nouvelle en la "drag & droppant" ou en appuyant sur le bouton **Media File** ce qui fera apparaître une boite de dialogue de selection de fichier.
- Enregistrez les modifications en cliquant sur **"Sauvegarder"** en bas

## Modifier les informations d'un karaoké ou d'une série

### Pour un karaoké

- Lancez Karaoke Mugen
- Allez dans **Système**
- Allez dans l'onglet **Karaokes** puis **Liste**
- Cherchez votre karaoké et appuyez sur le bouton d'édition à droite
- Effectuez vos modifications, supprimez des entrées à l'aide des petites croix sur les étiquettes pour ré-écrire par dessus.
- Enregistrez en cliquant sur **"Sauvegarder"** en bas

### Pour un tag

- Lancez Karaoke Mugen
- Allez dans **Système**
- Allez dans l'onglet **Tags** puis **Liste**
- Cherchez votre série ou tag et appuyez sur le bouton d'édition à droite
- Effectuez vos modifications, rajoutez des langues et des alias avec **Ajouter**, supprimez des entrées à l'aide des petites croix sur les étiquettes pour ré-écrire par dessus
- Enregistrez en cliquant sur **"Sauvegarder"** en bas

## Corriger des doubles lignes

Ce cas peut se produire si vous ne découpez pas assez vos paroles, qui sont donc trop longues et ne tiennent plus sur une ligne, mais deux (ou plus).

![ass](/images/creation/KaraokeDoubleLineEdit.jpg)

2 solutions sont possibles pour régler ça :

- Vous réduisez la taille de la police petit à petit jusqu'à faire rentrer votre double ligne sur une seule. Vous pouvez rentrer un nombre avec points. **Si, passé la taille 21 ou 20, vous avez toujours des doubles lignes, arrêtez, revenez à une taille normale, et passez à la 2e méthode si dessous.** Il ne faut pas non plus trop réduire la taille sinon les gens ne vont plus rien voir au moment de chanter, surtout à une certaine distance.

- Vous enlevez le script de décalage et vous découpez votre / vos double(s) ligne(s) en 2 lignes bien distinctes. Ainsi, ici, vous pouvez faire une ligne **"Cause I left behind the home"**, puis une 2e ligne **"that you made me"**

![ass](/images/creation/ChangeSizeFont.png)

## Corriger des triple lignes

Ce cas se produit lorsque l'une de vos lignes met trop de temps à disparaître pour laisser sa place à la supposée ligne suivante, ce qui fait que cette dernère est obligée de se mettre sur une 3ème ligne depuis le haut de l'écran.

![ass](/images/creation/KaraokeTripleLineEdit.png)

2 solutions sont possibles :

- Vous changez le découpage de vos lignes. Par exemple ici, vous pouvez très bien réctifier le tir et faire une première ligne **"kimi no sei, kimi no sei"** suivi d'une 2e ligne **"kimi no sei de watashi"**.

- Vous diminuez la durée de la ligne incriminée qui reste trop longtemps à l'écran. Par exemple ici, vous réduisez le temps de fin du premier **"kimi no sei"** afin que **"kimi no sei de watashi"** puisse prendre sa place comme il le devrait. Vous remarquerez cependant que cette méthode a un défaut comparé à la première : vous *pouvez* vous retrouver avec une ligne qui disparaît prématurément alors qu'elle n'a pas finie d'être complètement chantée.

## Gestion du script de fondu des lignes

Si votre karaoké commence **immédiatement**, votre première ligne de paroles risque d'apparaître avec bien trop peu de temps de préparation pour que vos invités se mettent à chanter.

C'est pourquoi il est conseillé dans ce genre de cas de supprimer **l'apparition en fondu** (et de laisser la disparition).

Prenez la première ligne avec l'effet "*fx*" et changez `fad(300,200)` en `fad(0,200)` et voilà, c'est tout. Votre ligne apparaîtra immédiatement et disparaîtra en 200 centième de seconde.

![ass](/images/creation/DeleteFad1Line.gif)

## Supprimer des styles inutilisés dans un fichier de paroles

Une fois votre karaoké terminé (et éventuellement corrigé), il est important de supprimer tous les styles que vous n'avez pas utilisé. Pour cela, dans Aegisub :

- Allez dans l'onglet **"Sous-titres" > "Gestionnaire de styles"**
- Dans la colonne **"Script actif"**, supprimez tout les styles finissant par `furigana` ainsi que ceux qui n'ont pas été utilisé (Down, Choir, Duo, etc).

![ass](/images/creation/DeleteScript.png)

## Après tout changement

Si vous avez changé une vidéo, vous devrez l'envoyer sur le FTP de Karaoke Mugen ou nous la faire parvenir [sur Discord](http://karaokes.moe/discord) avant d'envoyer vos autres fichiers modifiés.

Pour envoyer vos fichiers de données modifiés :

- Si vous n'avez pas accès au git de la base de karaokés, vous pouvez chercher votre chanson sur [kara.moe](https://kara.moe) et proposer votre modification directement dessus.
- Si vous avez accès au [git de la base de karaokés](https://gitlab.com/karaokemugen/bases/karaokebase) (vous êtes mainteneur) faites un **commit** avec vos fichiers `.kara`, `.ass` et/ou `.tag.json` s'ils ont été modifiés. Soyez **descriptif** sur les modifications effectuées. Une fois le **commit** fait, vous devrez le **push** pour l'envoyer. Pour faire un commit et push, allez dans la section Mainteneur puis Git du Panneau Système.

+++
title = "Références"
weight = 9
+++

Cette section n'est utile que si vous souhaitez envoyer votre chanson sur la base de données officielle de Karaoke Mugen. Si c'est pour vous-même, vous pouvez bien faire ce qu'il vous plaît.

Vous aurez besoin de ces informations pour créer vos karaokés :

## Sites web de référence

Quand vous éditez/ajoutez un karaoké, il est important d'avoir de bonnes références pour ne pas se tromper sur les noms des artistes / chanteurs / compositeurs / studio / etc.
Attention, certains d'entre eux utilisent une graphie particulière (avec des apostrophes ou des caractères spéciaux).

{{% include "includes/inc_series-names.fr.md" %}}

Nous sommes cependant [conscients](https://gitlab.com/karaokemugen/karaokebase/issues/417) que parfois, il y a des infos qui sont difficiles à trouver (des auteurs d'AMV des années 2000, des paroliers, etc). On ne vous fouettera pas sur la place publique si vous ne remplissez pas tout.

## Paroles

{{% include "includes/inc_lyrics.fr.md" %}}

## Japonais

{{% include "includes/inc_japanese.fr.md" %}}

## Tableau de taille des vidéos

{{% include "includes/inc_video-sizes.fr.md" %}}

## Studios / créateurs

Nock a mis en ligne un Google Sheet avec la liste la plus exhaustive possible des créateurs et studios d'animation notamment.

Quasiment tout les studios cités sont déjà dans la base de Karaoke Mugen.

Cette liste doit vous servir comme référentiel, si vous contribuez à une base différente de celle de KM, merci de garder cette liste en tête afin que vos karaokés et ceux de la base de KM utilisent les mêmes noms de studio.

[La liste est disponible via ce lien](https://docs.google.com/spreadsheets/d/1ULoVCi7UvTG0qSMVUhnPLOOiehH5jVrVYZMsuYi9O7s/edit#gid=18648527)

## Règles de transcription

{{% include "includes/inc_transcript-rules.fr.md" %}}
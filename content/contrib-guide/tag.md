+++
title = "Tag Files"
weight = 6
+++

## Tag types

Tags are metadata items allowing a better identification of a song. They allow the search engine to find the songs you want.

There are several kinds:

- Series
- Singers
- Songwriters
- Creators (studios, entity behind it)
- Karaoke authors
- Languages
- Song types (Ending, Opening...)
- Groups (2010s, Mainstream,...)
- Families (Anime, IRL, Video Game...)
- Origin (Mobage, TV series, Visual Novel...)
- Genres (Shounen, Shoujo...)
- Platforms (PS3, PS4, Gamecube...)
- Miscelleanous (Hard Difficulty...)
- Versions
- Warnings (Adults Only, Spoiler...)
- Collections (Geek, World...)

A tag must belong to one of those categories, and can belong to several ones (often singer and songwriter, for example...)

## Short names

For some tags, especially the misc. ones, platforms, genres, origins and types, a three letter name will be needed for Karoke Mugen.

For example: ANI stands for Anime, 360 for the XBOX 360, etc.)

## Aliases

Aliases allow the search engine to more easily find a song with this tag. For series, for example, you can add acronyms like FMA for Full Metal Alchemist, etc.

Adding words already in the various tag translations is useless though.

## No Live / No Downloads

Songs having this tag cannot be downloaded from the website or from Live.

## Priority

This is a number defining if a tag must appear before or after others in a list. For example ED and OP are two tags with a very low priority so they'll appear after all other song type tags if a song has several song types.

With a `-1` priority, the tag will be hidden from song lists in the public interface. At `-2` it is also hidden from view on the operator song lists.

## File tags

If a tag has a kara file tag set, it'll be used when naming kara files. For example "Audio only" has the "AUDIO" label.

## Description

These are the various descriptions (in various languages) of a tag. For now it's only used by maintainers or for collections.
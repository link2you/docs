+++
title = "Contributing to the database"
weight = 1
+++

If you are not a developper, and making a karaoke's super duper hard, there are many things to help with in the current database.

## Improve the database

What makes a great database is its cotent first of all. A database that can be sorted and scrutinised under many criterions is ideal. And we have by the way [a full section on the website](http://kara.moe) dedicated to this.

With this link, you can already know, for example, how many [songs in korean](https://kara.moe/karas/language/3b9e2749-27d0-4a78-873d-ea91a9ee9a25~5?p=0&filter=&order=search) there are in the database, or how many soundtracks [were written by Yoko Kanno](https://kara.moe/karas/songwriter/yoko%20kanno/af0be06d-ea2d-4f46-85d1-58cf9f42532a~8?p=0&filter=&order=search), or how many songs [are from the first PlayStation games](https://kara.moe/karas/platform/playstation/f84b68ac-943a-495a-9c6e-d90ec959b19e~13?p=0&filter=&order=search). This data is contained inside `.kara.json` files. We filled most of them with the data we knew, but there are a steady flow of new songs which could need updates.

* To fill in missing fields, several options: either you tell us what's missing or wrong in a song's metadata by contacting us and pray for someone to make the changes for you, or you sumbit those directly using the form down the karaoke's page.
* There are also [somewhat poorly synchronized songs](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=lyrics). That happens when lyrics don't appear at the right moment for singing, etc. If a karaoke is already in the list you can [fix the `.ass` file](../create-karaoke/karaoke.md) before [uploading it](../create-karaoke/upload). If it isn't the case you can go to [its page on kara.moe](http://kara.moe/kara?kid=08066ef4-6ef8-4e53-87a8-6a36c6459605) and use the link at the bottom to tell us more about it.
* We have also [poor quality videos](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=media), with small resolution or something alike. You need to search for a long time to find these particular videos, but sometimes some people keep those archived somewhere on their computers, so anyone can help! Do not hesitate to tell us if you can provide us with such files.
* We have a [suggestion list](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=suggestion)! If there's a particular song you like, issue a new suggestion and pray that someone will make the karaoke for you. The  **prioritary** suggestions are songs that are likely to be requested and sung at some event (as popular songs from the current anime season).
* Note if you want something done, do it yourself. That's why we have [a tutorial](../create-karaoke)!
* And if you think you found a video or lyrics with some mistakes, go [see the global list of issues](https://gitlab.com/karaokemugen/karaokebase/issues), someone may have signaled it before you. Otherwise, you can use [the song's page](http://kara.moe/kara?kid=08066ef4-6ef8-4e53-87a8-6a36c6459605) and use th elink at the bottom to tell is.

## Becoming a maintainer

If you're okay with helping us on the database from time to time, don't hesitate to [send us a message](https://mugen.karaokes.moe/en/contact.html), register on [our forum](https://discourse.karaokes.moe) or join our [Discord](https://karaokes.moe/discord). We love helping you to help us! We'll show you how to use Git and other tools.
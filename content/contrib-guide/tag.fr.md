+++
title = "Les fichiers tag"
weight = 6
+++

## Types de tags

Les tags sont des éléments de métadonénes d'un karaoké permettant de mieux l'identifier et d'affiner les recherches et regroupements.

Il existe différents types :

- Séries
- Chanteurs
- Compositeurs
- Créateurs (studios, entités créatrices)
- Auteurs de karaoké
- Langues
- Types de chanson (Ending, Opening...)
- Groupes (2010s, Mainstream, Génération Club Dorothée...)
- Familles (Anime, Réel, Jeu Vidéo...)
- Origines (Mobage, Série TV, Visual Novel...)
- Genres (Shônen, Shôjo...)
- Plateformes (PS3, PS4, Gamecube...)
- Divers (Difficile...)
- Versions
- Warnings (R18, Spoiler...)
- Collections (Geek, World...)

Un tag doit appartenir à l'une de ces catégories, et peut appartenir à plusieurs catégories (bien souvent chanteur et compositeur, par exemple.)

## Noms courts

Pour certains tags, notamment ceux divers, de plateformes, genres, origines et familles, il faudra indiquer un nom court à trois lettres pour l'interface web de Karaoke Mugen. Par exemple ANI pour Anime, 360 pour XBOX 360, etc.)

## Alias

Les alias permettent au moteur de recherche de KM de trouver un karaoké avec ce tag. Pour les séries par exemple, mettre ses acronymes est une bonen idée : FMA pour Full Metal Alchemist, etc.

Cela ne sert à rien de répeter des mots contenus dans les différentes traductions du tag, par exemple.

## Pas de téléchargements / Live

Les karaokés qui possèdent un tag avec ce paramètre ne peuvent pas être téléchargés depuis le site web ou lus par Live.

## Priorité

Il s'agit d'un nombre indiquant si un tag doit s'afficher devant un autre dans une liste. Par exemple ED et OP sont deux tags qui ont une priorité plus faible que les autres tags de type de chanson si un karaoké a plusieurs types de chansons.

Si la priorité est à `-1` le tag ne sera pas affiché dans la vue publique. A `-2`  même l'opérateur ne le verra pas dans la liste.

## Etiquette de fichier

Pour les noms de fichier, si un tag a une étiquette spécifique alors il sera pris en compte lors du nommage du kara. Par exemple "Audio uniquement" a comme étiquette "AUDIO".

## Description

Il s'agit de la description (en autant de langues que vous voulez) du tag. Il est censé uniquement aider les mainteneurs et est très peu utilisé pour le moment (pour les collections surtout)
+++
chapter = true
title = "Guide du contributeur"
weight = 2
+++

# Guide du contributeur

Cette section est là pour tout ce qui a trait à la création de karaokés, du rassemblement d'information et de matériel, la synchronisation des paroles jusqu'à ajouter votre chanson dans la base de données.

## Sommaire

{{% children depth="999" descriptions="true" %}}

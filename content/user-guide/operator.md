+++
title = "Karaoke Operator Guide"
weight = 8
+++

You're planning a karaoke session with a crowd that you're not too familiar with? Like, at an anime event? This guide is for you.

It aims at offer a checklist and ideas to better manage a public karaoke if you're not used to it.

Don't forget that it's only a guide and recommendation list. You'll need to adapt to your situation and what you're looking for. Karaoke is a matter of taste!

Of course, if you aim for a karaoke without any public interference, you can be the only one to manage the playlist. Some sections of this guide won't be for you.

You can [contribute to this guide by editing the file in the git repository](https://gitlab.com/karaokemugen/karaokemugen-docs/) 

## Prerequisites

Check the following things for your karaoke to be successful.

### Hardware

#### A computer, Mac or PC

The configuration required to make Karaoke Mugen work is a computer beefy enough to decode video and run a web application. Technically, any modern computer will do. However **we recommend you test the software itself on the chosen computer** before the event. If possible with a small group of friends.

#### A (big) screen

Don't forget that your users will need to see the top of the screen, and sometimes even the bottom for some songs which have a chorus displayed there.

#### Some powerful speakers

Your sound system needs to be right for the place you're going to use it in. This sounds obvious, but a reminder never hurts.

#### An internet connection

**If you wish for your users to connect to your instance and suggest songs, you'll need an internet connection.**

Since version 5.0, Karaoke Mugen allows people to access your karaoke remotely, without being on the same Wi-Fi network. You'll need a stable Internet (stability is better than capacity. A stable 10Mbps connection is better than unstable 100Mbps one). If you can do it, test this connection by letting some people access the karaoke session remotely.

A 4G USB key will do the job, if your cellular provider has good antennas in the area you choose for your karaoke.

#### Posters

Making a poster to promote your karaoke is a good idea: it's even better if it's displayed somewhere visible in your room. Add your instance's URL (with a QR Code if the poster size allows it) or use the remote access URL (`https://xxxx.kara.moe`) in Karaoke Mugen's options.

Reminding visitors how to connect and add songs to the karaoke might encourage them to have fun witht he app.

### Software

#### Keep it updated

Make sure you have the latest stable version, or a version you've tested well. To get the latest version, check out [the download page](https://mugen.karaokes.moe/en/download.html)

#### Update your karaoke database

The karaoke database is automatically updated at startup, so launch Karaoke Mugen the day before your event to make sure it's up to date.

##### Audio and video files

By default Karaoke Mugen will only download songs you add to the current playlist the moment you do it. **If you won't have an itnernet connection or if it's too slow/had quotas, you'll need all the media files pre-downloaded.** Or else you won't be able to play the songs your users requested.

If you've planned to use a predefined playlist, without opening access to the public, then there's no problem: make sure each playlsit you're going to use has been set to "current" at least once so Karaoke Mugen will download their songs. You can verify each song's download status by looking at their titles. If there's a little cloud icon next to them it means the song isn't downloaded yet.

**For a public session, it's recommended to have all medias pre-downloaded.** To do that, go into the system panel, click on the **Media downloads** tab and get everything by clicking on the **Synchronize** button.

{{% notice warning "Warning about disk space" %}}

All songs in the database make for several hundred gigabytes of data. We recommend you have a hard drive dedicated to Karaoke Mugen to store its medias.

{{% /notice %}}

You can change the media storage folder in the system panel, by modifying the **kara.moe** repository.

[Check out the documentation for more information](../getting-started)

## Configuration

Check the following items to configure your session just how you want it to. We'll see together which settings could be useful for you.

For each setting, check out [configuration documentation](../config) to get more information.

### Setting things up

#### Playlists

If you allow people to add karaokes, it's improtant to [separate the current playlist from the public playlist](../playlists/#playlists), so an operator can review suggestions and accept or refuse them. *We never said you shouldn't believe in your public, but you never know what could come out of it.*

#### Opening the interface

The public interface has three settings :

- **Opened** : Default situation, where people can add songs and search for songs.
- **Limited** : Only the current song information and playlist are displayed. Users cannot interact with it. It's useful if you don't want your user to add anything to the list, but still wish to let them see what's coming next.
- **Closed** : Nothing is available. Everything's closed. A page will tell your users the interface isn't currently available.

Check that the interface is in the right state before beginning.

#### Remote access

Karaoke Mugen's remote access can be setup from the options page. A 4-letter code will be given to your app on its first setup. You'll thus have a dedicated URL like this: `https://xxxx.kara.moe/` to allow anyone with Internet access to connect to your karaoke session. You can [ask us](https://mugen.karaokes.moe/en/contact.html) to get a custom code for a specific event if you absolutely want one. These custom codes are usually reserved to one of our patreon tiers.

#### Usage quotas

Karaoke Mugen allow you to control how many songs your users can add during your karaoke, either by:

- Number of songs
- Song duration

This is optional and you can totally remove all limits.

You might want to set this differently depending on your karaoke duration (1 hour, 4 hours, 20 hours...)

During a public karaoke, you might want to set it this way :

- 2 or 3 songs at once
- 4 minutes of karaoke

What does it mean?

This means that your users won't be able to suggest more than 3 songs. If they try to add another one, their quota will show 0 and they won't be able to suggest new songs.

Things that can give them back quota:

- One of their songs is displayed on screen or accepted by an operator from the suggestion list.
- 60 minutes have passed (this is configurable) since they added their song.
- A third (configurable) of connected users voted for one of their songs in the suggestion list.

For a "private" karaoke (where all additions are made directlyt ot he current list), this can help you avoid abuse.

For a "public" karaoke (where songs are added to the suggestion list first then validated by an operator) quota is less important, but can be annoying for the operator since they'll have a lot of suggestions to go through.

It's up to you to decide if you want to enable quotas or not.

#### Jingles interval

[Karaoke Mugen jingles](https://gitlab.com/karaokemugen/medias/jingles) are played every 20 songs (by default) so about 30 minutes of karaoke with only 1m30s songs.

Depending on your karaoke duration, you might want to lower that interval (number of songs) to see them appear more often.

There also are small introduction jingles (played when you start your karaoke), sponsors (like jingles), encores (played right before the last song), and outros (tu end the karaoke). You can configure all these.

#### Classic karaoke mode

This mode is useful for karaoke contests during anime events. When a song ends, a pause screen is displayed with the name of the person who requested the next song. Only the operator and that person can trigger the next song playing once they're ready.

#### Public song poll

You can enable the poll mode to let your users contribute to the playlist in another way.

Keep in mind that not everyone spends their life on their smartphone and that it might not be good during a public karaoke.

This mode is useful if you don't want to take care of the current playlist and let users or random decide which songs to play. Songs will be atuomatically added as time passes.

How does it work?

- A vote starts and is displayed on the users' devices' screen every time a song starts or during pauses when they're enabled.
- Four (configurable) songs are selected randomlyf rom the suggestion list (the one your users add songs to) 
  - Songs already in the current playlist aren't selected.
- Users have 30 seconds (configurable) to vote for one of the songs.
- When the delay expires or if we're getting near the end of the current song, the song with the most votes will be added to the current playlist.
  - If two or more songs are tied (even at 0 votes), a random song will be selected from the winners.
- A new vote starts at the beginning of the next song
- Songs which didn't get enough votes can be selected randomly again.

### Preparations

We're almost there!

#### Warm-up playlist

Your users will arrive during the session, and probably not at the very beginning. Creating a warm-up playlist can be useful : it can have a few songs (5-10 minutes) to attract people and give the karaoke room's mood until people start to add songs. Try not to put really popular songs so your public can add suggestions instead of singing the currently playing song!

Do not hesitate to add a few songs yourself if your users are busier singing than filling the playlist.

#### Blacklist and its criterias

Karaoke Mugen allows you to "hide" songs by keeping them from being selected by users. By adding some criterias, it'll autofill the blacklist so you can sing on your two ears (Yes, absolutely.)

A few examples of things you can add to blacklist criterias depending on your public:

- The "Adult Only" tag if you're in an all-ages event, to avoid someone adding a song that my hurt some sensibilities.
- The "Spoiler" tag to keep from... spoiling particular shows or movies by mistake. This depends on what you consider to be spoilery, of course.
- The "Epilepsy / PSE" tag to filter songs with quickly blinking visuals, which can cause issues with some people.
- You can also add a duration criteria, for example to keep away all songs longer than 2 minutes if you don't have a lot of time for your karaoke session.

What you want to forbid is entirely up to you. Note that if a song is present in the whitelist, it'll never be forbidden, even if a blacklist criteria could keep it hidden.

#### Last-minute verifications

- Do a sound test
- Verify everything's displayed on screen

## During the karaoke session

### Useful reminders

If you encounter performance issues (people complain that the app freezes or is slow to respond, or maybe the player has trouble switching to the next song, etc.), close the public interface and use pen and paper to note suggestions. Just like in old times.

If problems keep occuring, restart Karaoke Mugen.

Devs are always up for improving performance and optimize Karaoke Mugen, but depending on your computer and your situation, issues can still happen. Don't hesitate to [tell us about any problem you might have](https://mugen.karaokes.moe/en/contact.html)

### Announcements

When starting your session, explain briefly to your users how they can connect to your karaoke app to add songs and "like" suggestions from others. Talk about the poll system if you enabled it.

Note that during a karaoke, especially during a long session, your public can change: some people leave and others come. If you're taking pauses, try to remember to tell everyone about what you said at the beginning of the session. You can also display messages on screens or use posters in the room for that.

### Manage your playlist

Before anything else, remember that these are only tips and ideas you might have not thought about before.

It's also important to adapt to your public: being a good karaoke operator will require you to oberseve people. Experience is also important (like for a real DJ), and that's where you can stand out! Depending on the public at your event, you might want to use old anime openings, VTuber covers, visual novel openings or some really niche series. Or perhaps simply mecha or tokusatsu stuff.

- Keep in mind that a standard opening/ending is 1 min. 30 sec. long. Other songs (AMVs, MVs, etc.) will break the rhythm. We all have our preferences obviously, and having to wait longer than usal to be able to sing can frustrate more than one user. These long songs should be used with care.
  - Try to limit songs longer than 2 minutes
  - Niche or unknown songs can be used to lower the room's beat after a lot of popular songs where everyone sang. It'll allow their voices to rest. Be smart when adding those. This can also allow users to discover songs and anime they might not know.
  - After a calm or rather unknown song, play a very popular song to get people back on track.
  - Try to keep long karaokes apart from each other, at least by 10 minutes (7 to 10 openings/endings)
- Sometimes using the "Shuffle" button regularly can be useful:  
  - The playlist is shuffled randomly after the currently playing song (songs already played aren't shuffled, obviously)
  - This potentially allows a song placed later to be played sooner.
  - **BEWARE**: Some people are sometimes waiting for their song to play before leaving (for their train or flight, or maybe they want to go to sleep, etc.) and shuffling the playlist too often can have a frustrating effect for them, since the song they were waiting for can come much later.
  - Use the "balance" mode for shuffles to balance songs depending on the person who requested them. This mode will create a set of songs with the same requesters. For example if you have users A, B and C who added a song, it'll create a list by putting A's song, then B's song, then C's song, then another song from A, another song from B, etc. This is to make sure songs from everyone get a chance to play.
- Include songs in english (or the main language you speak at your event) along with not so popular songs or if you see people aren't singing as much as you'd expect. Depending on where you are, everyone can sing their native language freely. Old cartoons often work too. (like Ninja Turtles and so on.)
- Beware of your remaining time: Often keep an eye on the clock and keep a few slots open to add 2 or 3 final songs:
  - Otaku no Video
  - God Knows
  - Brave Love
  - Database / Raise your flag
  - Shinzô wo Sasageyo
  - Sono Chi no Sadame
  - etc.

If you have other tips and tricks about karaoke to share, don't hesitate to [contribute to this document](https://gitlab.com/karaokemugen/karaokemugen-docs/)!

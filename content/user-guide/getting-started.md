+++
title = "Usage"
weight = 2
+++

Once Karaoke Mugen has been successfully [installed](../install), you should be able to launch it from you Applications Menu, Start menu, Dock or whatever you call it.
Then, Karaoke Mugen should start.

## Let's go!

Here's what you should see in a window:

![Initialization](/images/user/Initialisation.png)

Congratulations, Karaoke Mugen is starting correctly. You can see many steps go through, it shouldn't take too much time (you can unfold the logs by clicking "Logs", duh.)

Database generation can be real quick if you don't have many songs, but with more than 10000 karaokes, it can take a few minutes, but that's not something you'll do very often. Depending on where the files are (SSD, HDD, or network share) it can take more or less time.

A series of questions will be asked to see if you want to create an online or local account. An online account can be used on all Karaoke Mugen apps (if you go to someone else's place to do some karaoke, for example, or on [kara.moe](https://kara.moe/base).)

Follow instructions on screen and everything should be fine.

![Screen accueil](/images/user/Screen accueil.png)

Once on the welcome screen, the app will try to update its various data from the internet, and send stats if you allowed it earlier.

From this welcome screen, you can then launch the tutorial, the operator or public interface, or configure the app, download songs and check out what's new, as well as the event log.

## First steps

Another window should also appear in the lower left corner of your screen with a wallpaper and connection info:

![Background](/images/user/background.png)

The URL at the bottom will allow your users to connect to the karaoke selection interface.

If you are connected to the Internet, users can access your karaoke to see the current playlist and suggest songs. **They can't see the karaoke in itself.** To share the video feed with them, we suggest you use Twitch or Discord.

If you aren't connected to the Internet, your local network's IP address will be displayed instead of the kara.moe address.

By default, the video player will play your karaokes in a small window so you can test things out. To get it to go full screen, you'll need to configure the app.

This windowed mode is perfect for tests or to have some background music while you work on something else.

## Interfaces

Karaoke Mugen has several interfaces depending on what you want to do. You can switch from one to the other via the "Go to" menu on the application window or from the welcome screen.

### Public interface

This is the interface for your guests/public. It allows them to search for songs, see playlists, and add songs to the public playlist. It won't allow you to control the player or manage your karaoke session.

You'll get to see this once you're logged in:

![Public interface example](/images/user/public.png)

If you log in from with your regular user account and have the operator role on this Karaoke Mugen app (which should be the case on your own), you'll land on the welcome screen first.

#### Restrict interface

By default the public interface is open: everyone can use all its features. However, when your karaoke nears its end, it could be interesting to restrict or close the interface to avoid people adding any new songs.

Three modes are available:

* Open: Users can add songs to the playlist
* Restricted: Users can only see the current song and the current playlist.
* Closed: well... closed. Nothing is possible.

### Operator interface

You can connect to it from the application's main window.

Apart from using the app's window, you can reach it using the following URL from your machine or another one : `http://abcd.kara.moe`. Check out the player's wallpaper to get connection URL.

You can easily manage the player from the operator panel and your various playlists. You can see here that we have a list of all the available songs on the left, and our current playlist on the right.

Try to add karaokes in the current playlist and click on Play!

![Admin interface example](/images/user/admin.png)

### System panel

From this interface you can :

* Configure karaoke sessions
* Read logs
* Configure advanced options
* Create and remove users
* Create and edit songs
* Create and edit various song metadata (tags)
* Update your karaokes
* Download new songs
* ...and many other things!

## Users

Every user can now have an account in the app. This account is local and bound to your karaoke instance. It contains a profile and the list of your favorite songs.
Online accounts are recognizable with the @ in their name. For example `axel@kara.moe`. The right-most part works like a mail address and identifies the Karaoke Mugen Server the compte is located at.

If you don't wish to create an account, you can click on "Continue as guest" so Karaoke Mugen can assign a guest account to you.
Guests cannot change their names and don't have a favorites list. They also can't *like* songs.

If you have a local account on your Karaoke Mugen, you can convert it to an online account from your profile.
You can also convert your account online to a local account: this will remove any possibility of connecting with another instance and will delete your account from Karaoke Mugen Server (`kara.moe`)

{{% notice note "Note for advanced users" %}}
`kara.moe` is made available to Karaoke Mugen users, but you can open your own Karaoke Mugen server to offer online accounts and a shortened URL of your choice (as long as you have the domain name). The server code source with deployment instructions are [available on GitLab](https://gitlab.com/karaokemugen/karaokemugen-server).
{{% /notice %}}

## Sessions

Sessions are karaoke moments. Parties, birthdays, events... Each session contains played and requested songs during it and can be helpful to make statistics, rankings, etc.You can select the active session from the welcome screen.You can also modify sessions from the system panel.

A session with the "private" flag won't be sent to Karaoke Mugen Server for stats.

+++
title = "Advanced Usage"
weight = 6
+++

This section is for all the technobabble and other stuff we talked about in various other sections.

## Folders

Karaoke Mugen stores its user data :

- In the `KaraokeMugen` folder at the root of your user folder (`C:\Users\<username>` on Windows, `/Users/<username>` on Mac, `/home/<username>` on Linux) if you used the Karaoke Mugen Installer.
- In the `app` folder at the root of the application if you used a portable package.

This is where you'll find the [configuration file](#configuration), as well as your downloaded songs, [the playlist medias](#playlist-medias), [backgrouns](#backgrounds), etc.

## Database

Karaoke Mugen stores its data in a PostgreSQL database in the `db/postgres` folder.

KM's binary package includes a PostgreSQL server launched along with the app, so you don't have to worry about it. You can still use your own PostgreSQL server if you'd like by following the instructions in [the configuration section](#configuration).

When updating Karaoke Mugen, the database is also u pdated automatically.

## Command-line

Here are the command line options for Karoke Mugen

* `--cli` : Starts Karaoke Mugen without any Electron window.
* `--config file` : Specify a config file to use. Replace `file` with any file you'd want to use instead of `config.yml`
* `--debug` : Displays debug messages in the console (these are normally only visible in the log files)
* `--dumpDB` : Creates a database backup in the `karaokemugen.sql` file and closes the app.
* `--forceAdminPassword <password>` : Change the admin account's password. Replace `<password>` by what you want to set it to.
* `--generate` : Generates a database then quits. This will overwrite the database if it exists.
* `--help` : Displays a help message
* `--kill` : Stops another KM app running.
* `--noAutoTest` : Used with `--test`, doesn't launch tests automatically after init.
* `--noBaseCheck` : Do not check base files on startup. It allows for a faster startup if you're sure your files have not changed.
* `--noMedia` : If used with `--generate` or `--validate`, this won't throw errors if video files can't be found. A single warning message will be displayed, at least.
* `--noBrowser` : Does not try to open a browser on launch.
* `--noPlayer` : Do not start up the player on startup to save some time. The player will be started by KM whenever you'll need it.
* `--profiling` : Displays profiling data in console
* `--reset` : Resets all user data. **Warning, this will remove EVERYTHING, users, playlists, etc.**
* `--restoreDB` : Restores a database backup from the `karaokemugen.sql` file and closes the app.
* `--sql` : Enables SQL logs (in debug)
* `--strict` : If used with `--generate` or `--validate`, this will throw errors if .kara files are modified during the generation. (this means one or more metadata are wrong/missing)
* `--test` : Runs in test mode, used with the unit tests ran via the `yarn test` command (only for devs.)
* `--updateBase` : Updates the karaoke base in your data folders and triggers a new database generation.
* `--updateMediasAll` : Only updates media files.
* `--validate` : Tests and validates karaoke found in the data folders (`app/data` by default). This is already done through generation. The only difference here is that it doesn't touch your existing database and can be faster.
* `--version` : Displays current version

## Configuration

Karaoke Mugen uses a configuration file called `config.yml` located in its [data folder](#folders).

If this file doesn't exist, it will revert to default settings. You can see which are these settings by reading the `config.sample.yml` file provided.

When you modify a setting via the [admin panel](web-interface) a `config.yml` file is written (or modified if it exists already) with the new settings, but not everything can be configured from the interface.

This section describes the app's config file. You shouldn't have to touch it yourself except for advanced use. You can edit most settings through the operator interface. More advanced options are available via the system panel.

### App

General settings are stored here

- `FirstRun` : Set to `false` by Karaoke Mugen to remember it's not the first time the application has been run. If set to false, tutorial won't be triggered, and admin password won't be reset.
- `JwtSecret` : Secret identifier used to salt user authentification tokens
- `QuickStart` : Boolean to tell if app should start quickly or not (disables base check)
    - This implies the app does not check your database on startup, which can lead to coherence issues. You should regenerate your database manually using the button in the system panel.

### Online

Online settings are stored here.

* `AllowDownloads` : Boolean allowing you to enable/disable song downloads. If it's disabled, the app won't download any media and will stream them instead
* `ErrorTracking` : Boolean allowing you to enable/disable error tracking. All error reports are sent to our [sentry.io](https://sentry.io) account.
* `FetchPopularSongs` : When `true`, the popular songs list in the public interface will try to fetch most popular songs from the different repositories you're connected to.
* `Host` : Karaoke Mugen Server hostname to connect to for online functionality. This will also replace the URL displayed on the player's screen. By default it's `kara.moe`.
* `MediasHost` : HTTP path where to seek medias if they can't be found locally. This is checked before the repository's URL, in case you want to have a local cache server for example.
* `Port` : Defines which port the app should use to connect to KM Server. Mainly used for debugging.
* `Remote` : Defines if the instance is available remotely via the proxy system defined in the `Host` option. This allows you to share access to your Karaoke Mugen instance without exposing ports on your network.
* `Stats` : Sends (or not) usage satistics to Karaoke Mugen Server.
     - `undefined` : User will be asked to make a choice on startup
	 - `false` : No data will be sent.
	 - `true` : Stats will be sent.
* `Users` : Allows user to create and login online accounts.
	  - `false` : Users will only able to use local accounts. if an online account is already created on your instance, its name will become `user@something.com`. For example `axel@kara.moe`.
	  - `true` : Users can connect with their online accounts.

#### Discord

* `DisplayActivity` : Boolean. Displays your karaoke activity on your Discord profile. It'll show if you're singing or not and on which song.

#### Updates

Defines what is going to be automatically updated :

All values are at `true` by default.

* `App` : Checks if an update is available.

##### Medias

* `Intros` : Should intros be updated automatically from Shelter's Gitlab?
* `Jingles` : Same with jingles.
* `Encores` : Same.
* `Outros` : Same.
* `Sponsors` : Same.

### Frontend

Web interface settings

* `Mode` : Interface mode
	- `0` : Interface is closed to all users except admins
    - `1` : Interface is restricted. Users cannot add songs anymore and can only see the currently playing song and playlist.
	- `2` : Interface fully operational (default)
* `SeriesLanguageMode` : Display mode for language of series names
    - `0` : Series will have its original title (in japanese romaji for an anime)
	- `2` : Series will have its name depending on system language (fallback to english, etc.)
	- `3` : Series will have its name depending on user's browser language (...)
* `ShowAvatarsOnPlaylist` : Boolean. Displays avatars or not on playlists in the interface.

#### Permissions

* `AllowNicknameChange` :
	- `false` : Users cannot change nicknames
	- `true` : Users can change their nicknames (default)

### GUI

* `OpenInElectron` :
    - `false` : All links clicked in the Electron window will open a browser.
	- `true` : All links clicked in the Electron window will remain in there.

#### ChibiPlayer

* `Enabled` : Boolean
* `AlwaysOnTop` : Boolean
* `PositionX` : Number
* `PositionY` : Number

#### ChibiPlaylist

* `Enabled` : Boolean
* `PositionX` : Number
* `PositionY` : Number

### Karaoke

Karaoke session specific settings

* `AutoBalance` : When adding a song to the current playlist, Karaoke Mugen will try to apply the "balanced" shuffle method to the playlist.
* `Autoplay` : If no song is being played, playback will start as soon as a song is added to the list.
    - `false` : Starting playback is manual (default)
	- `true` : Playback automatically starts if a song is added
* `ClassicMode` : If enabled, playback will stop after each song, allowing only the admin or the user who requested the next song to start playing again from their device.
    - `false` : Classic mode disabled
	- `true` : Classic mode enabled
* `CreatePreviews` :
	- `false` : No preview video will be created
	- `true` : At startup, Karaoke Mugen will verify if some songs need to have their video preview generated. If some previews are missing, KM will create them with FFMPEG. This can last for a while seeing how many songs with have in the database at the moment. Preview creation cannot be interrupted, but if you close KM and start it again, it will resume where it left off.
* `MinutesBeforeEndOfSessionWarning` : When a session has its end time set, a warning will be displayed on the operator interface X minutes before its end.
* `Private` :
	- `false` : Public karaoke. User submissions go to the public playlist.
	- `true` : Private karaoke. User submissions go to the current playlist. (by default)
* `ProgressBarDock` : (boolean) Displays a progress bar for playback and database generation in the macOS Dock or Windows task bar.
* `Repeat` :
    - `false` : Playback stops when reaching the end of the current playlist (by default).
    - `true` : Playback continues by looping through the playlist.
* `SmartInsert` :
	- `false` : Smart Insert is disabled.
	- `true` : Smart Insert is enabled when in `private` mode. this will favor people who haven't added many songs, for example.

#### Collections

Here are stored your enabled/disabled collections (as Tag IDs).

Example:

```yaml
Collections:
  c7db86a0-ff64-4044-9be4-66dd1ef1d1c1: true # Otaku/Geek
  efe171c0-e8a1-4d03-98c0-60ecf741ad52: true # World
  2fa2fe3f-bb56-45ee-aa38-eae60e76f224: true # Shitpost
```

#### ConnectionInfo

* `Enabled` :
    - `false` : No QR Code or URL will be displayed during song playback.
    - `true` : Connection information is displayed (default)
* `QRCode` : Display QR Code between pauses
    - `false` : No
    - `true` : Yes
* `Message` : Message to display next to the interface URL. Examples :   	- 1 euro per song!
    - Wifi network: Jonetsu
* `Host` : If empty, your IP address will be automatically detected, but this detection can sometimes fail : this setting allows you to force a specific IP to use.

#### Poll

* `Enabled` : Public votes
    - `false` : No
    - `true` : Yes
* `Choices` : Number of songs to suggest in the poll
* `Timeout` : Poll duration. Poll will end at this many seconds, or before the last 10 seconds of a song.

#### Quota

A freed song doesn't count anymore towards a user's quota.

* `Songs` : Number of songs someone can suggest in public or private mode.
* `Time` : Time bank in seconds for every user
* `Type` : Quota type to use. This is checked when someone tries to add a song.
    - `0` : No quota
	- `1` : Number of songs
	- `2` : Time bank
* `FreeAutoTime` : Time in minutes before a song is automatically freed (60 mintues by default)
* `FreeUpVotes` :
	- `false` : Likes won't allow users to get their songs freed.
    - `true` : Users who have a "liked" song get it freed from their quota.
* `FreeUpVotesRequiredPercent` : Percentage (0-100) of likes required to get a song freed. It's the percentage of people marked as connected to the app at a given time.
* `FreeUpVotesRequiredMin` : Minimum number of likes required for a song to be freed.

#### StreamerMode

The Streamer Mode displays poll results during song pauses, and can also allow Twitch chatters to submit their votes. It also adds a configurable pause inbetween songs.

* `Enabled` : `true` or `false` depending on if you want that mode or not
* `PauseDuration` : In seconds.

##### Twitch

* `Enabled` : `true` or `false` depending on if you're using Twitch Chat or not
* `OAuth` : Your OAuth token for Twitch
* `Channel` : Twitch channel for the bot to join

### Player

* `Background` : Name of an image file to use for the player's background.
    - If nothing is specified, Karaoke Mugen will choose a random background in the `app/background` folder.
	- If no background is found in the folder, the default one will be used.
* `Borders` :
    - `false` : The window has no borders (looks smaller)
	- `true` : The window has borders and a titlebar.
* `ExtraCommandLine` : String added to mpv's command-line. Only use if you know what you're doing.
* `FullScreen` : This setting is ignored if `PIP.Enabled` is set to `true`.
	- `false` : Player is in windowed mode (default)
	- `true` : Player is in fullscreen
* `HardwareDecoding` :
    - `no` : Hardware decoding disabled
	- `yes` : Force hardware decoding. Depending on your hardware, drivers, and the video played, it could have negative effects or even crash the player.
	- `auto-safe` : Default. Only enable on known working combinations of video codecs and hardware. Check mpv documentation to know more
* `LiveComments`:
    - `false` : Disable live comments on screen
	- `true` : (default) Enable live comments on screen. This is only usable with Twitch.
* `Monitor` :
    - `false` : Monitor is disabled (default)
	- `true` : The monitor is displayed. It's a second player window synced with the first one. It allows the karaoke operator to see what's being played on the main screen.
* `mpvVideoOutput` : You can specify a video driver for mpv here. If under Windows you see a blue window when launching Karaoke Mugen, put `direct3d` in this.
* `NoHud` :
	- `false` : mpv's HUD is enabled
	- `true` : mpv's HUD is hidden (default)
* `NoBar` :
	- `false` : mpv's progress bar is displayed when you seek inside the video.
	- `true` : mpv's progress bar is never displayed.
* `Screen` : screen number on which mpv should display your karaoke.
	- `0` : first screen (main screen) (default).
	- `1` : second screen.
	- `2` : third screen.
	- ...
	- `9` : Let the player choose
* `StayOnTop` :
	- `false` : mpv won't stay on top of other windows
	- `true` : mpv will stay on top of other windows (default)
* `Volume` : From 0 to 100, mpv's starting volume.

#### Display

* `Avatar` : Display a user's avatar in the bottom right corner with song information on screen.
    - `false` : Do not display avatar picture
	- `true` : Display avatar picture (`Nickname` needs to be `true`)
* `Nickname` :
	- `false` : Player won't display nickname and avatar
	- `true` : Player will display nickname of the person who requested a song.
* `RandomQuotes` :
    - `false` : Random quotes aren't displayed when the KM version banner is displayed
	- `true` : (default) Random quotes are displayed
* `SongInfo` :
    - `false` : No song information is displayed at the beginning and end of a song. If this is disabled, `Avatar` and `Nickname` aren't used.
	- `true` : (default) Song information is displayed
* `SongInfoLanguage` : A [ISO639-2B language name](https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes). In addition of the standard ones, KM also supports `qjr` for Japanese Romaji.

#### PIP

* `Enabled` :
	- `false` : Disables Picture in picture mode
	- `true` : Enables Picture in picture mode (default)
* `Size` : if PIP mode is enabled, this is the percentage of the screen in size the player window should cover.
* `PositionX` : Defines mpv's horizontal position by default
	- `Left`
	- `Center`
	- `Right`
* `PositionY` : Defines mpv's vertical position by default
	- `Top`
	- `Center`
	- `Bottom`

### Playlist

* `AllowDuplicates` : A song can be added more than once if this setting is set to `true` (only on non-public playlists)
* `EndOfPlaylistAction` :
	- `none` : Do not do anything, karaoke ends.
	- `random` : Play random songs from the library
	- `repeat` : Go back to the beginning of the playlist
* `MaxDejaVuTime` : A song is marked as "deja vu" if it's been played less than xx minutes ago (60 by default)
* `RandomSongsAfterEndMessage` : Si activé, affichera un message invitant les gens à ajouter des chansons à la liste de lecture courante lorsque l'action de fin de playlist est `random`.


#### MysterySongs

* `Hide` :
    - `false` : Mystery songs are visible (default)
	- `true` : Mystery songs are hidden, they won't appear in a playlist to the public
* `AddedSongVisibilityAdmin` :
    - `false` : Songs added by admin are set to mysterious automatically
	- `true` : Songs added by admin are visible
* `AddedSongVisibilityPublic` :
    - `false` : Songs added by public are set to mysterious automatically
	- `true` : Songs added by public are visible

#### Medias

For every item there are three settings :

* `Enabled` : Enable or disable the media with a boolean.
* `File` : (Not for jingles and sponsors) Filename to use for that media during playback. If none is set, KM will select a file at random.
* `Interval` : (Jingles and sponsors only) Number of songs before displaying these
* `Message` : An optional message to display on the screen while the media plays

For sponsors and jingles :

* `Interval` : Number of songs before that media is played

Medias can be `Intros`, `Outros` and `Encores`. Example :

```YAML
    Intros:
      Enabled: true
      File: PS1-Nanami.mp4
    Outros:
      Enabled: false
      File: Byebye.mp4
    Encores:
      Enabled: false
      File: encoru.mp4
```

### System

- `FrontendPort` : Network port to use for KM's interface.

#### Binaries

These are the paths to various binaries needed by Karaoke Mugen. Each binary has three settings : `Windows`, `OSX` and `Linux`. Binaries are  `Player`, `ffmpeg`, `Patch` and `Postgres`. For `Postgres` it's the folder where all binaries should be available. For the others, an executable is expected.

Example :

```YAML
System:
  Binaries:
    Player:
      Linux: /usr/bin/mpv
      OSX: app/bin/mpv.app/Contents/MacOS/mpv
      Windows: app/bin/mpv.exe
```

#### Database

How to access your database. You can usually leave all the defaults. If you use your own PostgreSQL server, put `bundledPostgresBinary` to `false` and modify whatever is needed (like the password). `superuser` and `superuserPassword` aren't used in that case.

```YAML
System:
  Database:
    bundledPostgresBinary: false
    database: 'karaokemugen_app'
    host: 'localhost'
    password: 'musubi'
    port: 5432
    schema: 'public'
    superuser: 'postgres'
    superuserPassword: null
    user: 'karaokemugen_app'
```

Everything's pretty explicit. Edit the settings you need to.

#### MediaPath

* `Jingles` : Folders where to look for jingles.
* `Intros` : Folders where to look for intros.
* `Sponsors` : Folders where to look for sponsors
* `Outros` : Same thing for outros
* `Encores` : ... and encores videos!

#### Repositories

All your repositories are configured here.

##### Example repository `kara.moe` :

* `Enabled` :
    - `true` : Repository will be used for generation and online updates
	- `false` : Repository will NOT be used for generation and online updates. Its songs won't appear in database anymore.
* `Name` : Repository name. Must match the name in the `.kara.json` and `.tag.json` files present. For online repositories this has to be a working domain name.
* `Online` :
	- `true` : The repository is an online one and will be queried during online updates
	- `false` : The repository is local and only contains local songs not meant to be uploaded to an online repository.
* `BaseDir` : Base directory where your online repository will be downloaded, folder where `karaokes`, `lyrics`, `tags` folders can be found.
* `MaintainerMode` : Repository maintainer mode, used to tell that the repository is managed by another software, which prevents Karaoke Mugen updating the repo.
* `AutoMediaDownloads` : Define the behavior of media downloading
	- `all` : As soon as a new video is available, it will be downloaded. As such, all the base is available without Internet.
	- `updateOnly` : (by default) No video will be downloaded, but if a video you already downloaded was updated on remote, it will be updated locally.
	- `none` : No video will be downloaded. Everything will be manual.
* `Path` : Paths for each file type :
	- `Medias` : Path to medias files, they will be downloaded to these folders.

#### Path

Some paths here can have multiple entries. These are `Backgrounds`, `Jingles`, `Karas`, `Lyrics`, `Medias` and `Tags`. All those paths are relative to the [data folder](#folders) but you can still specify absolute paths.

When searching for a file in these, Karaoke Mugen will go through every entry of the list.

* `Avatars` : Folder where avatars are stored
* `Backgrounds` : Folder where backgrounds are stored.
* `BundledBackgrounds` : Folder where KM will extract its bundled backgrounds. This is cleaned up on each startup.
* `DB` : Folder where the database(s) are stored.
* `Import` : Folder where karaokes can be imported from
* `Previews` : Folder where previews are stored (about 500Kb per song)
* `Temp` : Temporary folder. It is cleaned on startup.
* `SessionExports` : Path where to store Session .csv exports.
* `StreamFiles` : Path where to store text files for streaming

## Customization

In addition to the various customization settings you'll find in the config file, you can change a few other things in Karaoke Mugen to fit your needs :

### Wallpapers

By default, Karaoke Mugen will use its own wallpaper depending on which version you're using. You can still specify yours through the backgrounds page in the system panel.

Karaoke Mugen will display them randomly at every pause.

### Pause Music

By default there's no background music bundled with Karaoke Mugen. You can put your own in place though by going through the backgrounds page in the System Panel. Most formats (mp3, m4a, ogg, flac, wav...) are accepted. They'll be read during pauses between two songs.

If a song has the same name as a picture file (without its extension) it'll be read as soon as the image file will be selected. If no music is found with the same name, a random one will be choosen.

### Playlist medias

If you want to put a little spice in your karaoke, you can play jingles or sponsors every XX songs (via the [`Medias`](#medias_1) settings). There are also introductions, outros, and _encores_. All these videos are stored in the [data folder](#folders)

The Karaoke Mugen community has a few jingles they share, but you can use yours. If an `.ass` file with the same name as a media file is present, it'll be played along.

These medias can be downloaded [on our Gitlab](https://gitlab.com/karaokemugen/medias). Place them in their respective folders once unzipped. They are automatically downloaded by Karaoke Mugen, too.

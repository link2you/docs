+++
title = "Installation"
weight = 1
+++

This page explains how to get and install Karaoke Mugen.

**NOTE :** Once installed, Karaoke Mugen does not need any internet connection, except for some features :

- Remote access for the public through the URL https://xx.kara.moe
- Online accounts
- Song updates on startup
- Most popular song lists
- Playing songs **which haven't been downloaded locally yet**

You should have Internet access though on your first launch, just to get the latest song database.

## Download Karaoke Mugen

Binaries for Windows, macOS and Linux are available. You'll find either installers or portable installs. **If you don't know which to pick, we recommend the installer version.**

[Go to the downloads page](http://mugen.karaokes.moe/en/download.html)

Extract the .zip archive in a folder of your choice where there's a lot of disk space available.

## Install Karaoke Mugen

There are several ways to install depending on if you want the app from source or from compiled versions ready to use.

Once done, you can go to the [getting started](../getting-started) section!

### Windows

Two choices :

#### Installer

Double-click on the executable and follow instructions

#### Portable

Extract the .zip archive in a folder/drive where you have enough space available.

### macOS

Double-click on the DMG image and drag & drop Karaoke Mugen in your application folder.

### Linux

No matter which distribution you're using, we recommend using the **AppImage** version, which should be self-contained.

Linux support, while present, depends on a lot of things (each distribution and installation is unique). If you run into issues, don't hesitate to [contact us](https://mugen.karaokes.moe/contact.html).

#### Debian/Ubuntu package (.deb)

The package should create an entry in your application menu.

#### ArchLinux

There are two AUR packages, [`karaokemugen`](https://aur.archlinux.org/packages/karaokemugen) and [`karaokemugen-git`](https://aur.archlinux.org/packages/karaokemugen-git/). The first is based on the latest stable version and the second on the last commit from the `master` git branch. They both can be installed in the same way.

##### Installation

```Shell
$ pikaur -S karaokemugen-git
```

_Installation can take some time depending on your configuration, from 10 to 20 minutes._

##### Initial configuration

As you saw, the AUR package comes with the `karaokemugen-install` utility, allowing you to automatically configure Karaoke Mugen! Launch this utility as soon as install is over.

```Shell
$ karaokemugen-install
```

**Warning:** as said during launch, this script will probably not work if you changed the default PostgreSQL configuration. This script will need _super-user_ rights several times via _sudo_.

At launch, the script warns you of potential problems which could keep it from working and then does all necessary actions to make Karaoke Mugen work (create database, apply configuration, etc.)

This script is interactive. It may ask you a few questions on what to do (especially if a previous Karaoke Mugen database is found) and other configuration items.

![karaokemugen-install](/images/setup/km42.jpg)

Once done, you're ready to launch Karaoke Mugen.

##### Launch

Congratulations! Your Karaoke Mugen install is ready to start so you can experience incredible adventures.

You should be able to find Karaoke Mugen in your  Applications menu's desktop environment. If that's not the case, launch it simply with `karaokemugen` in a terminal.

![App Launch](../img/setup/km44.png)

![Karaoke Mugen](/images/setup/km43.jpg)

##### It doesn't work?

First, sorry. We want the install to be as simple as possible but we can't think of everything. Don't hesitate to [contact us](../about/contact.md) if you have a problem.

## Install from source

You can install Karaoke Mugen by downloading its source code.

- Download a Karaoke Mugen version from [its git repository](https://gitlab.com/karaokemugen/karaokemugen-app) through `git clone` or the ZIP archive provided by gitlab.
	- Each version is tagged : pick the version [you want to use](https://gitlab.com/karaokemugen/karaokemugen-app/tags).
	- If you like risks you can [download the dev version](https://gitlab.com/karaokemugen/karaokemugen-app/repository/next/archive.zip). **Beware, it most probably has bugs!**
- Unzip/place the Karaoke Mugen source code where there's enough free space.
- Download [mpv](http://mpv.io) and place its executable in the `app/bin` folder or specify its path in the [config file](configuration) if you already have mpv installed on your system.
	- Version 0.33 or later required
- Download [ffmpeg](http://ffmpeg.org) and place the `ffmpeg` executable in the  `app/bin` folder.
	- Version 3.3.1 is the minimum requirement
- Download [nodeJS](https://nodejs.org/en/download/) which is required for Karaoke Mugen to run.
	- Minimum version required is 16. Do not use a newer major version.
	- Install nodeJS once it's been downloaded
- Download [GNU Patch](https://savannah.gnu.org/projects/patch/) if your distribution doesn't have the required version (see below.) It is required for song database patches
    - Minimum version required is 2.7
- Download [PostgreSQL](http://postgresql.org)
    - Minimum version required is 13.x.
	- You'll need to configure your database server for Karaoke Mugen to work. Modify the `config.yml` file to point to your created database. See [configuration](advanced/#configuration). Check out the app's README to see which commands to run on the database
- Open the command line (cmd, terminal, bash...) and go into the Karaoke Mugen folder. Then launch :

If `yarn` is not installed yet on your system, go to [its website](https://yarnpkg.com) to install it.

Once `yarn` is installed :

```sh
yarn setup
```

`yarn` will install all necessary modules for Karaoke Mugen to work and build the React client. It can take from 1 to 5 minutes depending on your system's speed and your internet connection.

Once done, you can type `yarn start` to launch the app. You should then go to the [getting started](getting-started.md) section.

## Platform-specific information

### Linux

You'll need PostgreSQL installed and ready to use. [Refer to the installation from sources section](#install-karaoke-mugen-from-source)

#### Linux (Ubuntu)

On older Ubuntu releases (before 22.04) the version of mpv provided by the repositories is obsolete, and **not compatible with Karaoke Mugen**.

You must thus install another version. [mpv's official site](https://mpv.io/) provides several ways to install Karaoke Mugen, but the simplest is for Ubuntu and its derivatives to use the following PPA :

```sh
sudo add-apt-repository ppa:mc3man/mpv-tests
sudo apt-get update
sudo apt-get install mpv
```

Check out [mpv's website](http://mpv.io) for more info.

### macOS

#### Patch

The patch version bundled with macOS is too old, you'll need to use [Homebrew](https://brew.sh) to install it.
